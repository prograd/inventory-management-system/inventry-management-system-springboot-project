package com.example.InventoryManagementSystem.security;

import com.example.InventoryManagementSystem.advice.CannotAccessId;
import com.example.InventoryManagementSystem.entities.Customer;
import com.example.InventoryManagementSystem.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class CustomerDetailsImpl implements UserDetailsService {
    @Autowired
    private CustomerRepository customerRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Customer customer=this.customerRepository.findByCustomerEmail(username).orElseThrow();
        return customer;
    }
}
